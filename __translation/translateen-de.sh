#!/bin/bash

curl -X POST "https://lindat.mff.cuni.cz/services/translation/api/v2/models/en-de?src=en&tgt=de" -H  "accept: application/json" -H  "Content-Type: application/x-www-form-urlencoded" \
--data-urlencode "input_text=$1"
