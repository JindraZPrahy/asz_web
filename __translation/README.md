# How to translate the whole website

The english version is treated as the reference version and
the german is just a translated copy.

1. Execute
```
find "$(cd ../en; pwd)"  -maxdepth 6 -type d >dirstotranslate.txt
```

in this folder.

2. Replace /en/ with /de/ for example using vim

3. Call `./TranslateAll.sh`

## How it works

The script `translateen-de.sh` translates strings it receives as an argument.
It uses the [lindat](https://lindat.mff.cuni.cz/services/translation/docs)
model.

The script `translateHTML.py` translates individual HTML documents using
the Python HTML parser.
It may be a bit difficult to understand, because the comments are written in Czech
and the parsing is done a bit haphazardly to support some weird things
that aren't even in use on this website. But it gets the job done.

The script `TranslateAll.sh` calls `translateHTML.py` for every specified folder.
