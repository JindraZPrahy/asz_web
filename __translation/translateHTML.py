from subprocess import Popen
import subprocess
from html.parser import HTMLParser
import re #Regex Searching
import sys #Abych mohl skript volat z terminálu

path='~/asz_web/__translation'
inputfile='index.html'
outputfile='index.html'
#inputfile =sys.argv[1]
#outputfile=sys.argv[1]


### Tento program obdrží inputfile ve formátu html
### a vrátí jej přeložený z čj do aj pomocí API překladače Lindat.
# https://lindat.mff.cuni.cz/services/translation/api/v2/dochttps://lindat.mff.cuni.cz/services/translation/api/v2/doc
### Bohužel se smažou všechny bold tagy.
### Taky předpokládám, že vložení hashtagu # do překladu jako slovo nezmění
### zbytek překladu. Toto je nutné, aby fungovaly hyperlinky.
### To samé o ***
### Taky předpokládám, že se nerozbije inline matematika $x^2$.
### Tag $$ musí být pouze jeden v paragrafu, a to sice na konci.

### K samotnému překladu je používána funkce translate, která volá bash skript,
### který volá api.

### Pro použití skriptu je potřeba udělat: export PATH=$PATH:path/to/this/dir



### Překládací funkce
#  input = string česky
# output = string anglicky
def translate(string):
	print(string)
	proc = Popen(path+"/translateen-de.sh \"%s\"" % string, shell=True,stdout=subprocess.PIPE)
	stdout=proc.stdout.read()
	translated=str(stdout)[3:-8]
	translated=translated.replace('",','').replace('"','').replace("\\\'","\'")
	return translated

### Odstraní přebytečné mezery a newliny z pole <pre>
def delete_whitespace(pole_textu):
	while pole_textu[0] == '\n ':
		pole_textu.pop(0)
	while (pole_textu[0][0:1] == '\n' or pole_textu[0][0:1]==' '):
		pole_textu[0]=pole_textu[0][1:]
	while pole_textu[-1] == '\n ':
		pole_textu.pop(-1)
	while pole_textu[-1][-1] == '\n':
		pole_textu[0]=pole_textu[0][0:-1]
	pole_textu.append('\n')  #Aby bylo alespoň nějaké formátování
	pole_textu.insert(0,'\n')
	for i in range(0,len(pole_textu)): #Artefakt po smazání některých code blocků
		pole_textu[i] = pole_textu[i].replace("-->","")



### Příprava dokumentu
# 1. Odstranění řádků a tabulátorů
with open(inputfile, 'r') as file:
	data = file.read().replace('\t',' ')

# 2. Odstranění nbsp
data=data.replace('&nbsp;',' ')

# 3. Příprava odkazů
data=re.sub(r'(<a.*?>)',r'\1# ', data)
data=data.replace('</a>',' #</a>')
data=re.sub('\\$\\\\LaTeX\\$', 'LaTeX', data)

# 4. Odstranění tučného písma a kurzívy
data=data.replace('<strong>','')
data=data.replace('</strong>','')
data=data.replace('<b>','')
data=data.replace('</b>','')
data=data.replace('<em>','')
data=data.replace('</em>','')

# 5. Code blocky
# Odstranění code blocků uvnitř pre blocků
data=re.sub("<pre>.*\n.*<code contenteditable spellcheck=\"false\">.*\n",'<pre>',data, re.DOTALL) #Multiline regexp
data=re.sub('</code>.*\n.*</pre>.*\n','</pre>',data, re.DOTALL)
# Nastavení pro in-line code blocky
data=re.sub(r'(<code.*?>)',r'\1*** ', data)
data=data.replace('</code>',' ***</a>')

# 6. Odstranění uvozovek
data=data.replace("„",'')
data=data.replace('”','')

# 7. Odstranění br
data=data.replace("<br>",'')

# 8. Odstranění center
data=data.replace('<center>','')
data=data.replace('</center>','')

# 9. Práce s matematikou
# Nejdřív z $$ udělám code blocky <matematika>
while True:
	data=data.replace('$$', '<matematika>',1)
	data=data.replace('$$','</matematika>',1)
	if not '$$' in data:
		break
# Na konci to zase vrátím

# 10. '
data=data.replace("'","\\'")

#print(data)


### Zápis

f = open(outputfile,"w")

### Parser
Paragraf  = []
Odkazy = []
codeno=0
ismath=0
isstyle=0
mathtxt = []

# create a subclass and override the handler methods
class MyHTMLParser(HTMLParser):
	def handle_starttag(self, tag, attrs):
		global Paragraf, Odkazy, codeno, ismath, isstyle
		attrs=str(attrs)
		attrs=attrs.replace("[",'').replace("]",'').replace("\'href\', ",'href=').replace("(",' ').replace(")",'') # pro a
		attrs=attrs.replace(",  \'class\',", ' class=').replace("\'class\', ",'class=') # pro div
		attrs=attrs.replace("\'style\', ",'style=').replace("\'src\', ",'src=').replace("\'alt\', ",'alt=') # pro <img>
		attrs=attrs.replace("\'contenteditable\', ","contenteditable").replace('None,','').replace("\'spellcheck\', ",'spellcheck=') # pro <code>
		attrs=attrs.replace("\',","\'") # pro nepovedené věci
		tag=str(tag)
		prvnitaga=str("<"+ tag+ attrs+">")
		if (tag=="p" or tag=='h1' or tag=='h2' or tag=='h3' or tag=='h4' or tag=='li' or tag=="pre" or tag=="title" ): #Ukládání vnitřního textu pro tyto pro tyto
			Paragraf  = []
			Odkazy  = []
		if (tag=='a'): #Ukládám si znění všech odkazů
			Odkazy.append(prvnitaga)
		elif (tag=='img'): #Odsadit obrázek
			f.write("<br><br>\n<"+ tag+attrs+">\n<br><br>\n")
		elif (tag=='code'): #Netiskni nic
			codeno=codeno+1
		elif (tag=='matematika'): #Netiskni nic
			ismath=1
		elif (tag=='style'): #Netiskni nic
			isstyle=1
			f.write("<"+ tag+attrs+">\n")
		elif (tag=='link'): #Netiskni nic
			f.write("<"+ tag+attrs+">\n")
		else:
			f.write("<"+ tag+attrs+">\n")

	def handle_endtag(self, tag):
		global Paragraf, Odkazy, codeno, ismath,isstyle, mathtxt
		tag=str(tag)
#		print(mathtxt)
		if (tag=='p'or tag=='h1' or tag=='h2' or tag=='h3' or tag=='h4' or tag=='li' or tag=='pre'): #Ukládání vnitřního textu pro tyto
			if tag=='pre':
				delete_whitespace(Paragraf)
				tisk=''.join(Paragraf) # preformatted => without deleting \newline
				f.write(str(tisk))
				f.write("\n</"+ tag+">\n")
				return
			tisk=''.join(Paragraf).replace("\n",'')
			tisk=translate(tisk)
			for i in range(0, len(Odkazy)): #Doplnění odkazů po překladu
				tisk=tisk.replace("# ", Odkazy[i], 1)
				tisk=tisk.replace(" #", "</a>", 1)
			for i in range(0, codeno): #Doplnění odkazů po překladu
				tisk=tisk.replace("*** ", '<code contenteditable spellcheck="false">' , 1)
				tisk=tisk.replace(" ***", "</code>", 1)
			tisk=tisk.replace("index.php", "index.php?lang=en").replace("\\\\","\\").replace("\\\\","\\") #Nevím proč to zdvojuje lomítka... # Odkazy na angickou versi jidu
			tisk=tisk.replace("\\u2013",'—').replace("\\u2192",'→').replace('\u010d','č').replace('\u00e9','é')
			tisk=tisk.replace('\\u00f6','ö').replace('\\u00e4','ä').replace('\\u00fc','ü').replace('\\u00dc','Ü').replace('\\u00df','ß')
			f.write(tisk)
			if (len(mathtxt)!=0):
				f.write('\n$$'+mathtxt[0]+'$$')
				mathtxt.pop(0)
				mathtxt.pop(0)
			f.write("\n</"+ tag+">\n")
			ismath=0
		if (tag=='div' or tag=='pre' or tag=='figure' or tag=='ol' or tag=='ul' or tag=="title"): #Bez ukládání vnitřního textu
			f.write("\n</"+ tag+">\n")
		if (tag=='style'):
			f.write("\n</"+ tag+">\n")

	def handle_data(self, data):
		global Paragraf, Odkazy, ismath,isstyle, mathtxt
		if ismath==1:
			mathtxt.append(data)
		elif isstyle==1:
			f.write(data)
			isstyle=0
		else:
			Paragraf.append(data)
#		print(data)

# Inititate the parser and feed it some HTML
parser = MyHTMLParser()
parser.feed(data)

f.close()

##Some post-hoc editing
# Read in the file
with open(outputfile, 'r') as file :
  filedata = file.read()

# Replace the target string
filedata = filedata.replace("'rel' ", "rel=")
filedata = filedata.replace("'type' ", "type=")

# Write the file out again
with open(outputfile, 'w') as file:
  file.write(filedata)

